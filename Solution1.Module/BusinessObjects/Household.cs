﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace Solution1.Module.BusinessObjects
{
    [DefaultClassOptions]
    public class Household : XPObject
    {
        public Household(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        XPCollection<AuditDataItemPersistent> _auditTrail;


        string name;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                SetPropertyValue("Name", ref name, value);
            }
        }

        string address;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                SetPropertyValue("Address", ref address, value);
            }
        }

        string city;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string City
        {
            get
            {
                return city;
            }
            set
            {
                SetPropertyValue("City", ref city, value);
            }
        }

        string state;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string State
        {
            get
            {
                return state;
            }
            set
            {
                SetPropertyValue("State", ref state, value);
            }
        }

        string zip;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Zip
        {
            get
            {
                return zip;
            }
            set
            {
                SetPropertyValue("Zip", ref zip, value);
            }
        }

        string phoneNumber;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string PhoneNumber
        {
            get
            {
                return phoneNumber;
            }
            set
            {
                SetPropertyValue("PhoneNumber", ref phoneNumber, value);
            }
        }

        string email;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                SetPropertyValue("Email", ref email, value);
            }
        }

        string description;

        [Size(2048)]
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                SetPropertyValue("Description", ref description, value);
            }
        }

        [Association]
        public XPCollection<Person> People
        {
            get
            {
                return GetCollection<Person>("People");
            }
        }

        [Association, ExplicitLoading]
        public XPCollection<Tag> Tags
        {
            get
            {
                return GetCollection<Tag>("Tags");
            }
        }

        public string AppliedTags
        {
            get
            {
                if (Tags != null)
                {
                    return string.Join(", ", Tags.Select(t => t.Name).ToArray());
                }
                else
                {
                    return "N/A";
                }
            }
        }

        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (_auditTrail == null)
                    _auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return _auditTrail;
            }
        }

    }
}