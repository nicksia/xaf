﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Linq;

namespace Solution1.Module.BusinessObjects
{
    [DefaultClassOptions]
    public class Tag : XPObject
    {
        public Tag(Session session) : base(session)
        { }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Active = true;
        }

        string name;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                SetPropertyValue("Name", ref name, value);
            }
        }

        string description;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                SetPropertyValue("Description", ref description, value);
            }
        }

        bool active;
        public bool Active
        {
            get
            {
                return active;
            }
            set
            {
                SetPropertyValue("Active", ref active, value);
            }
        }


        [Association]
        public XPCollection<Household> Households
        { 
            get
            {
                return GetCollection<Household>("Households");
            }
        }


    }
}