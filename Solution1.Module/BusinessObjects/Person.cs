﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Linq;

namespace Solution1.Module.BusinessObjects
{
    [DefaultClassOptions]
    public class Person : XPObject
    {
        public Person(Session session) : base(session)
        { }

        string firstName;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                SetPropertyValue("FirstName", ref firstName, value);
            }
        }

        string lastName;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                SetPropertyValue("LastName", ref lastName, value);
            }
        }

        [NonPersistent]
        public string FullName
        {
            get
            {
                return String.Format("{0} {1}", FirstName ?? "", LastName ?? "");
            }
        }

        DateTime birthDate;
        public DateTime BirthDate
        {
            get
            {
                return birthDate;
            }
            set
            {
                SetPropertyValue("BirthDate", ref birthDate, value);
            }
        }

        string phoneNumber;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string PhoneNumber
        {
            get
            {
                return phoneNumber;
            }
            set
            {
                SetPropertyValue("PhoneNumber", ref phoneNumber, value);
            }
        }

        string email;
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                SetPropertyValue("Email", ref email, value);
            }
        }


        Household household;
        [Association]
        public Household Household
        {
            get
            {
                return household;
            }
            set
            {
                SetPropertyValue("Household", ref household, value);
            }
        }


    }
}
