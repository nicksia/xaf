﻿namespace Solution1.Module.Controllers
{
    partial class TagController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.setTagAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // setTagAction
            // 
            this.setTagAction.ActionMeaning = DevExpress.ExpressApp.Actions.ActionMeaning.Accept;
            this.setTagAction.Caption = "Set Tag";
            this.setTagAction.Category = "Edit";
            this.setTagAction.ConfirmationMessage = null;
            this.setTagAction.Id = "SetTagAction";
            this.setTagAction.ImageName = "BO_Contact";
            this.setTagAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.setTagAction.TargetObjectType = typeof(Solution1.Module.BusinessObjects.Household);
            this.setTagAction.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;
            this.setTagAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.setTagAction.ToolTip = "Set Tag";
            this.setTagAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.setTagAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.setTagAction_Execute);
            // 
            // TagController
            // 
            this.Actions.Add(this.setTagAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction setTagAction;
    }
}
