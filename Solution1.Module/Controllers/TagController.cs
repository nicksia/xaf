﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using Solution1.Module.BusinessObjects;

namespace Solution1.Module.Controllers
{
    public partial class TagController : ViewController
    {
        public TagController()
        {
            InitializeComponent();
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            var objectSpace = Application.CreateObjectSpace();
            var tags = objectSpace.GetObjects<Tag>(new BinaryOperator("Active", true))
                                  .Select(t => new ChoiceActionItem(t.Name, t))
                                  .ToArray();

            setTagAction.Items.Clear();
            setTagAction.Items.AddRange(tags);
        }

        private void setTagAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            var items = e.SelectedObjects;
            var tag = e.SelectedChoiceActionItem.Data;

            if(items.Count > 0 && tag is Tag)
            {
                var objectSpace = Application.CreateObjectSpace();
                var t = objectSpace.GetObject<Tag>((Tag)tag);
                foreach (object o in items)
                {
                    if (o is Household)
                    {
                        var h = objectSpace.GetObject<Household>((Household)o);
                        h.Tags.Add(t);
                    }
                }

                objectSpace.CommitChanges();
                View.ObjectSpace.Refresh();
            }
        }
    }
}
